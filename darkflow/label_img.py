# -*- coding: utf-8 -*-
"""
Based on Razan's test_cnn.py

"""

import cv2
import matplotlib.pyplot as plt
import glob
import os
from pascal_voc_writer import Writer
import re
import pandas as pd
import numpy as np
if __name__=="__main__":
    basepath = os.getcwd()
    ## creating VPC format labels for the files
    labels=sorted(glob.glob(basepath+'/labels/*'))
    images=sorted(glob.glob(basepath+'/images/*'))
    for img,filename in zip(images,labels):
        label_file=pd.read_csv(filename, sep=" ", header=None, names=['xmin','ymin','xmax','ymax','label'])
        im=cv2.imread(img)
        height=im.shape[0]
        width=im.shape[1]
        writer = Writer(img, width, height)
        for idx, label in label_file.iterrows():
            tag=label['label']
            if tag == 1:
                name='unhealthy'
            if tag == 0:
                name='healthy'
            xmin=label['xmin']
            xmax=label['xmax']
            ymin=label['ymin']
            ymax=label['ymax']
            writer.addObject(name, xmin, ymin,xmax,ymax)
            filename = os.path.splitext(os.path.basename(img))[0]
        writer.save(basepath+'/new_labels/'+filename+'.xml')
