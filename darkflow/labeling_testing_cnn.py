# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 10:19:59 2020

@author: Razan
"""

import cv2
import matplotlib.pyplot as plt
import glob
import os
from pascal_voc_writer import Writer
import re
from darkflow.net.build import TFNet


#import tensorflow as tf
#print(tf.__version__)
#tf.__version__tf


basepath = os.getcwd()

print(basepath)



## test everything working
#options = {
# 'model':    f"{basepath}/cfg/yolo-mine.cfg",
# 'load':     f"{basepath}/bin/yolov2.weights",
#  "config":  f"{basepath}/cfg/",
# 'threshold': 0.3
#}
#
#tfnet = TFNet(options)
#
#img = cv2.imread(f'{basepath}/new_data/images/train-healthy-44.jpg', cv2.IMREAD_COLOR)
#img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
#dimensions = img.shape
#
#
#
#result = tfnet.return_predict(img)
#result

## ploting result 
#for i in range(0, len(result)):
#    tl = (result[i]['topleft']['x'], result[i]['topleft']['y'])
#    br = (result[i]['bottomright']['x'], result[i]['bottomright']['y'])
#    label = result[i]['label']
## add the box and label and display it
#    img = cv2.rectangle(img, tl, br, (0, 255, 0), 7)
#    img = cv2.putText(img, label, tl, cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 0), 2)
#    plt.imshow(img)
#plt.show()



## Training on a new dataset using Yolov2

##reshaping images to (height, width,dim) (319, 450, 3)
height = 319
width = 450
dim = (width, height) 

inputFolder = f'{basepath}/new_data/train/healthy'
folderLen = len(inputFolder)
os.makedirs('Resized')
for img in glob.glob(inputFolder+"/*.jpg"):
    print(img)
    image = cv2.imread(img)
    imgreszied = cv2.resize(image, dim)
    cv2.imwrite("Resized" + img[folderLen:], imgreszied)
    cv2.waitKey(30)
    
cv2.destroyAllWindows()

## creating VPC format labels for the files
path = f'{basepath}/new_data/train/unhealthy Resized'

for img in glob.glob(path+"/*.jpg"):
    imfile = cv2.imread(img, cv2.IMREAD_COLOR)
    dimensions = imfile.shape
    width =  dimensions[0]
    height = dimensions[1]    
    xmin = int(width*0.25) 
    xmax = int(width*0.75)
    ymin = int(height*0.25)
    ymax = int(height*0.75)    
    writer = Writer(img, width, height)
    writer.addObject('unhealthy', xmin, ymin, xmax, ymax)
    filename = os.path.splitext(os.path.basename(img))[0]
    writer.save(path+'/'+filename+'.xml')

path = f'{basepath}/new_data/train/healthy Resized'

for img in glob.glob(path+"/*.jpg"):
    imfile = cv2.imread(img, cv2.IMREAD_COLOR)
    dimensions = imfile.shape
    width =  dimensions[0]
    height = dimensions[1]    
    xmin = int(width*0.25) 
    xmax = int(width*0.75)
    ymin = int(height*0.25)
    ymax = int(height*0.75)    
    writer = Writer(img, width, height)
    writer.addObject('healthy', xmin, ymin, xmax, ymax)
    filename = os.path.splitext(os.path.basename(img))[0]
    writer.save(path+'/'+filename+'.xml')
    
    
##new model training
    
options = {"model": f"{basepath}/cfg/yolo-mine.cfg", 
           "load":  f"{basepath}/bin/yolov2.weights",
           "batch": 4,
           "epoch": 5,
           "train": True,
           "config":     f"{basepath}/cfg/",
           "annotation": f"{basepath}/new_data/annots/",
           "dataset":    f"{basepath}/new_data/images/"}

tfnet = TFNet(options)

tfnet.train()



